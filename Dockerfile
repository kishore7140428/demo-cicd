FROM openjdk:11
ADD build/libs/gradleSampleArt.jar demo-ci.jar
EXPOSE 8097
ENTRYPOINT ["java", "-jar","demo-ci.jar"]